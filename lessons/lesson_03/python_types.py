# %%
type(4)

# %%
type(4.0)

# %%
type("4 - int, 4.0 - float")

# %%
type(print)

# %% [markdown]
"""# Приведение типов"""

# %%
20 / 5

# %%
type(20 / 5)


# %%
print(float(4))
print(str(2))
print(int(1.5))


# %%
# int отбрасывает дробную часть числа
print(int(4.6))
print(int(-4.6))


# %%
# Строгая типизация. Следующая строка выполнится с ошибкой
print("1" + 2)


# %%
print(1.0 + 2)


# %%
# В Python встроена поддержка комплексных чисел
vector_one = 5 + 3j
vector_two = 1 + 4j
print(vector_one + vector_two)


# %%
# У объектов есть значение, тип и уникальный id
a = 4
b = 5
c = 4
# id объектов a и c совпадают, т.к. a и c ссылаются на один объект
print(id(a), id(b), id(c))
