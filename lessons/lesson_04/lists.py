# %%
import math
# %%
a = range(20)

# %%
b = iter(a)

# %%
next(b)

# %%
c = list(range(20))
c
# %%
numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 11, 12, 13]
# %%
numbers = sorted(numbers, reverse=True)
# %%
numbers.sort(reverse=False)
# %%
numbers
# %%
del numbers
# %%
numbers

# %%
numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 11, 12, 13]

# %%
square_numbers = [num**2 for num in numbers if num % 2 == 0]

# %%
my_shiny_list = ["abc", "привет", 1, -5]
# %%
[str(elem) for elem in my_shiny_list]

# %%
list(map(str, my_shiny_list))

# %%
list(map(math.sin, [1, 2, 3, 4]))

# %%
people = {
    "yaroslav": {"age": 22, "height": 173, "weight": 85},
    "zaynulla": {"age": 32, "height": 189, "weight": 72},
}
# %%
people["zaynulla"]["age"]
# %%
