"""Set of functions to simulate power budget."""

# %%
import math

import numpy as np
from mission_switcher import *


# %%
def power_income(sun_visible_part: float, e_sun_b: np.ndarray) -> float:
    """Determining the income of electricity from solar panels

    Parameters
    ----------
    sun_visible_part (float):
        Sun visible part. 0 if sun is not visible, 1 if fully visible, 0..1 partially visible.
    e_sun_b (np.ndarray):
        Sun direction unit vector in body reference frame.

    Returns
    -------
    float:
        Power income
    """

    res = 0
    if sun_visible_part > 0:
        for i in range(3):
            res += (
                SOLAR_PANELS_MAX_POWER[i][round(0.5 + math.copysign(0.5, e_sun_b[i]))]
                * abs(e_sun_b[i])
                * sun_visible_part
            )
    return res
