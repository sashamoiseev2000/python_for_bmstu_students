import pytest
from power_budget import SOLAR_PANELS_MAX_POWER, power_income

power_income_test_data = [
    pytest.param(0, [1, 0, 0], 0, id="Sun is not visible"),
    pytest.param(
        0.5,
        [1, 0, 0],
        0.5 * SOLAR_PANELS_MAX_POWER[0][1],
        id="Sun visible from x+ side 1/2 part",
    ),
    pytest.param(
        1, [1, 0, 0], SOLAR_PANELS_MAX_POWER[0][1], id="Sun visible from x+ side"
    ),
    pytest.param(
        1, [0, 1, 0], SOLAR_PANELS_MAX_POWER[1][1], id="Sun visible from y+ side"
    ),
    pytest.param(
        1, [0, 0, 1], SOLAR_PANELS_MAX_POWER[2][1], id="Sun visible from z+ side"
    ),
    pytest.param(
        1, [-1, 0, 0], SOLAR_PANELS_MAX_POWER[0][0], id="Sun visible from x- side"
    ),
    pytest.param(
        1, [0, -1, 0], SOLAR_PANELS_MAX_POWER[1][0], id="Sun visible from y- side"
    ),
    pytest.param(
        1, [0, 0, -1], SOLAR_PANELS_MAX_POWER[2][0], id="Sun visible from z- side"
    ),
]


@pytest.mark.parametrize(
    "sun_visible_part, e_sun_b, expected_value", power_income_test_data
)
def test_power_income(sun_visible_part, e_sun_b, expected_value):
    assert power_income(sun_visible_part, e_sun_b) == expected_value
